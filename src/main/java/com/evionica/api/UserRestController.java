package com.evionica.api;

import com.evionica.dto.User;
import com.evionica.service.RepositoryUserFinder;

/**
 * This is main REST controller.
 */
public class UserRestController {

    private final RepositoryUserFinder userFinder;

    public UserRestController(RepositoryUserFinder userFinder) {
        this.userFinder = userFinder;
    }

    public User findUserByEmail(String email) {
        return userFinder.findUserByEmail(email)
                .orElseThrow(() -> new RuntimeException("user not found"));
    }
}
