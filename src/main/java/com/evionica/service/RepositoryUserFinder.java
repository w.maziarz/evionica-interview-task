package com.evionica.service;

import com.evionica.dto.User;
import com.evionica.repository.UserRepository;

import java.util.List;
import java.util.Optional;

public class RepositoryUserFinder {

    private final UserRepository userRepository;

    public RepositoryUserFinder(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Optional<User> findUserByEmail(String email) {
        List<User> allUsers = userRepository.findAll();

        User foundUser = null;
        for(User user : allUsers) {
            if(user.getEmail() != null && user.getEmail().equals(email)) {
                foundUser = user;
            }
        }

        return Optional.ofNullable(foundUser);
    }
}
