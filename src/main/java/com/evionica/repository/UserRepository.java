package com.evionica.repository;

import com.evionica.dto.User;

import java.util.List;

/**
 * This interface CANNOT be modified. Assume that it is a kind of library
 * that we have no access to.
 */
public interface UserRepository {

    List<User> findAll();

}
